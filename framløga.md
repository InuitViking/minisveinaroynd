---
marp: true
paginate: true
theme: uncover
style: |
  /*Til hele sliden*/
  section {
    @import url('https://fonts.googleapis.com/css2?family=David+Libre&family=Oxygen+Mono&display=swap');
    background-color: #EFEFEF;
    color: #202124;
    font-family: 'Oxygen Mono', monospace;
  }
  /*Slide animation*/
  @keyframes marp-transition-slide-out{
    from{
      width: 100%;
    }
  to{
      width: 0%;
    }
  }
  @keyframes marp-transition-slide-in{
    from{
      width: 0px;
    }
  to{
      width: 100%;
    }
  }
  .list {
    width: 100%;
  }
  /*Til headers*/
  h1, h2, h3, h4, h5, h6, p, .list {
    overflow-x: hidden;
    display: inline-block;
    white-space: nowrap;
    margin: 0 auto;
    animation: marp-transition-slide-in 1.5s;
  }
  h1 {
    font-weight: 700;
  }
  /*Til texten indeni (eller H2)*/
  h2 {
    font-weight: 400;
  }
  h5 {
    text-align: left;
    margin: 0;
  }
  /*Til pagination*/
  section::after {
      content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
      background: none;
      font-family: 'David Libre', serif;
      display: block;
      color: #555555;
      height: 40px;
      min-width: 100px;
      padding: 4px;
      text-shadow: none;
      text-align: center;
  }
  a.source {
    position: absolute;
    bottom: 0;
    left: 0;
    color: #2F384A;
    font-size: 24px;
  }
  code, pre {
    background-color: #BFBFBF;
    border-radius: 10px;
  }
---
# Linux og containere
#### Angutivik Casper Rúnur Tausen
EUC Syd - 15.06.2022

<!-- Hej
Jeg hedder Casper.
Jeg vil snakke om Linux containerisation, hvordan det fundamentalt virker, og små værktøjer vi kan bruge som programmører, systemadministratorer, og devops.

Jeg kommer ikke til at snakke om microservices, men vi kommer til at dykke ned i det lavpraktiske, og hvordan vi kan påvirke selve Linux kernelen på bit-niveau. -->

---
![bg right:36%](https://cdn.discordapp.com/attachments/640153863034503171/986325357131075585/Screenshot_from_2022-06-14_19-44-28.png)
<div class="list">
  <ul>
    <li>Containerisation
      <ul>
        <li>Containere vs. VMs</li>
        <li>Linux kernel features
          <ul>
            <li>Namespaces</li>
            <li>Control Groups</li>
          </ul>
        </li>
      </ul>
    </li>
    <li>Områder</li>
    <li>Konklusion</li>
    <li>Brug i projektet</li>
  </ul>
</div>

<!--
- Containers vs. VMs (recap)
- Linux kernel features
  - Namespaces
  - Control Groups
- Områder det bliver brugt
- Generel konklusion
- Brug i projektet -->

---
# Containerisation
## Containere vs. VMs

---
![bg w:95%](https://cdn.discordapp.com/attachments/640153863034503171/986338441430175765/Containers.png)
![bg w:95%](https://cdn.discordapp.com/attachments/640153863034503171/986338441765740594/Virtualization.png)
<a class="source" href="https://www.redhat.com/en/topics/containers/containers-vs-vms" target="_blank">Kilde: Red Hat - Containers vs VMs</a>

<!-- ## Containere
Containere er i bund og grund isolerede processer som kører bestemte opgaver. Processerne er ofte begrænset på en eller anden måde.
Siden containere kun er isolerede processer, så er der næsten ingen overhead når det kommer til hardware.
Containere kører på den host de er blevet startet på, og er ikke en virtualisering af et andet operativsystem, men bruger hellere den kernel der er på hosten.

## VMs
Virtuelle maskiner er større entiteter og simulere operativsystemere på virtuelt hardware. Det vil sige at vi laver en virtuel maskine med sit eget OS, med egen kernel og software. At ændre resurserne on-the-fly er ikke altid understøttet, fordi både hypervisoren og gæste-OSet skal understøtte det. -->

---
# Containerisation
## Linux Kernel Features

---
##### Namespaces og Control Groups
<div class="list">
  <ul>
    <li>Namespaces
      <ul>
        <li>Isolerer processer fra hinanden</li>
      </ul>
    </li>
    <li>Control Groups
      <ul>
        <li>Kontrollerer ressourcer</li>
      </ul>
    </li>
  </ul>
</div>

<!-- Det vi får ud af det her er, at vi kan isolere processer (med sub-processer), give dem specielle rettigheder indenfor deres namespace, samt begrænse de resourcer de må bruge.
Samlet kan vi sørge for at programmer og almindelige brugere kun eksisterer i et isoleret sted, med begrænsede ressourcer. Hvis der sker et sikkerhedsbrist i et namespace, så ved vi at det højest sandsynligt ikke kommer ud over namespacet. -->

---
![bg right:30%](https://cdn.discordapp.com/attachments/640153863034503171/986405521034735726/Screenshot_from_2022-06-15_00-59-06.png)
<h2 style="margin: 0 !important; text-align: left !important;">Namespaces</h2>
<div class="list">
  <ul>
    <li>Mount</li>
    <li>Process ID</li>
    <li>Network</li>
    <li>Interprocess Communication</li>
    <li>UTS</li>
    <li>User ID</li>
    <li>Control Group Namespace</li>
    <li>Time Namespace</li>
  </ul>
</div>

<!-- Der findes 8 typer namespaces:
- Mount
  - Kontrollerer mount points af filsystemer.
  - Giver os mulighed for at give specielle rettigheder til et filsystem ved hjælp af permissions, hvilket kan styres med bitmasking.
  - Hvis ét filsystem bliver unmounted, påvirker det ikke de andre.
- Process ID
  - Giver processer PIDer baseret på andre namespaces.
  - Laver en process med PID 3 et namespace, har den PID 1 i sit namespace.
  - Alle processer denne process må generere i dette namespace, vil have inkrementelle PIDer baseret på forældreprocessens PID.
  - På denne måde kan vi dræbe en forældreprocess, som samtidig dræber alle den under processer.
- Network
  - Dette namespace virtualiserer netværksstacken, hvilket per default kun er en loopback.
- Interprocess Communication
  - Dette er en måde for processer at have delte filer og delte memory segments, hvilket de kommunikerer over IPC.
  - Dette er også en måde for processer at lave pipes, altså vi har en "write ende" til at skrive bytes og en "read ende" til at læse bytes, hvilket gøres i FIFO orden.
- Unix Time-Sharing
  - Dette gør det muligt for et system at iklæde sig en ny identitet, så som hostname og domain name, for forskællige processer.
- User ID
  - Privilege isolation og User Identification segregation på tværs af processer
  - Er efterkommer af dét user namespace der lavede det.
  - I en container, så har root brugeren uid 0, men i systemet har den reelt uid 1.400.000 for at holde styr på egerskabchecks.
- Control Group Namespace
  - Dette namespace gemmer identiteten af en proces control group.
- Time Namespace
  - Dette namespace giver processer muligheden for at se andre tidspunkter.
  - Lidt lige som UTS 
Alt det her er grunden til at vi kan have isolerede systemer, altså containere, både til programmer og brugere som vi ikke stoler helt på.
-->

---
![bg right:30%](https://cdn.discordapp.com/attachments/640153863034503171/986406426782076958/Screenshot_from_2022-06-15_01-06-42.png)
<h2 style="margin: 0 !important; text-align: left !important;">Control Groups</h2>
<div class="list">
  <ul>
    <li>Egenskaber
      <ul>
        <li>Ressourcebegrænsning</li>
        <li>Prioritering</li>
        <li>Regnskab</li>
        <li>Kontrol</li>
      </ul>
    </li>
    <li>Kan styre mellem
      <ul>
        <li>Individuelle processer</li>
        <li>Virtualisering af OSer</li>
        <li>RAM som kernel må bruge</li>
      </ul>
    </li>
  </ul>
</div>

<!-- Egenskaber:
- Ressourcebegrænsning for processer
  - Max CPU forbrug
  - Max RAM forbrug
  - Højest read og/eller write
- Prioritering af processer
  - Så som brug mest mulig RAM eller mere I/O throughput
- Regnskab
  - Overvågning af forbrug
- Kontrol
  - Frysning af processer, checkpointing, og genstart
-->

---
# Områder
<div style="width: 50.9%; margin: 0 auto;">
  <ul>
    <li>Programmering</li>
    <li>Systemadministration</li>
    <li>DevOps</li>
  </ul>
</div>

---
![bg right:40%](https://cdn.discordapp.com/attachments/640153863034503171/986407155173306368/Screenshot_2022-06-15_at_01-09-09_udovin_WilcotCpp_A_C_library_that_helps_in_developing_Linux_applications.png)
<h2 style="margin: 0 !important; text-align: left !important;">Programmering</h2>
<div class="list">
  <ul>
    <li><code>libcgroup-dev</code></li>
    <li><code>wilcot</code></li>
    <li>Docker</li>
  </ul>
</div>

<!-- I programmering kan vi bruge libcgroup-dev (i C og C++ programmering) til at styre ressourcerne på de processer vores program vil starte. Derudover kan vi med wilcot lave containere til disse processer, så vi har et program der kører sikkert og begrænset uden at påvirke hele systemet, skulle noget gå galt.
Docker kan vi bruge som isolerede arbejdsmiljø til hvert et projekt.
Der er mange flere ting som giver os muligheden for at bruge cgroup og Linux namespaces.

De bedste programmeringssporg til namespaces og cgroups, i form a tilgængelige libraries, er C, C++, og Go, men hvis et programmeringssprog understøtter at snakke med en shell, kan også det gøres den vej. -->

---
![bg right:40%](https://www.wagex.org/wp-content/uploads/2019/02/tasks.png)
<h2 style="margin: 0 !important; text-align: left !important;">Sysadmin</h2>
<div class="list">
  <ul>
    <li>Linux hypervisors</li>
    <li>Docker</li>
    <li><code>libcgroup</code></li>
    <li><code>libcgroup-tools</code></li>
  </ul>
</div>

<!-- I systemadministration vil man højest sandsynligt bruge det i forbindelse med virtualisering, måske også docker, men man kan også bruge det direkte til at begrænse bestemte processer og brugere på et Linux system.
Når du styrer en VM, sætter du nogle virtuelle hardware limits på, og dette gøres indirekte med namespaces og cgroups, bare til en langt større proces end hvis man brugte docker.
libcgroup-tools er en sjat værktøjer til at styre cgroups direkte på systemet. Du kan også styre det vha. `nice` commandoen, eller ved nyere Linux maskiner, ofte med systemd. -->

---
![bg right:40%](https://www.2daygeek.com/wp-content/uploads/2018/03/dry-an-interactive-cli-manager-for-docker-containers-8-1024x680.png)
<h2 style="margin: 0 !important; text-align: left !important;">DevOps</h2>
<div class="list">
  <ul>
    <li>Docker</li>
    <li>CI/CD</li>
  </ul>
</div>

<!-- Ved hjælp af docker og andre containerisation systemer, bruger devops højest sandsynligt docker og laver ofte en del continuous integration og continuous deployment. De bruger indirekte Linux namespaces og cgroups.
Disse to ting hjælper især i CI/CD, hvis der er en bug i f.eks. koden, som gør at RAM forbruget går udover en grænse, eller at den konstant skriver til en fil som bare bliver større og større, så kan vi sætte grænser på, så at forbruget ikke går over gevind, og at det er isoleret så det ikke påvirker andre systemer. -->
---
## Konklusion
<div class="list">
  <ul>
    <li>Grundlaget for containers i Linux</li>
    <li>Vi kan alle bruge det</li>
  </ul>
</div>

<!-- Uden Linux namespaces og cgroup kom vi ikke langt med containere i Linux. Hele idéen med containere er at isolere enkelte processer, så de kun har bestemte rettigher, bestemte adgange til bestemte filsystmer og foldere, osv.

Derudover er det noget vi alle kan gøre brug af, både i programmering, systemadministration og DevOps. -->
---
### Brug i projektet
<div class="list">
  <ul>
    <li>Kubernetes</li>
    <li>DevOps</li>
    <li>CI/CD</li>
    <li>Yderligere udvikling</li>
  </ul>
</div>

<!-- Men hvad med projektet vi fik udleveret?
- I vores projekt beskriver vi at vi vil bruge Kubernetes, hvilket i bund og grund bruger docker containere.
- Yderligere, ikke ligefrem beskrevet i projektet, men en uundgåelig del af udviklingen produktet, så ville der blive taget brug DevOps og CI/CD. Da disse også bruger containere til at deploye ændringer i produktion, bruger vi nemlig også namespaces og cgroups.
- Endvidere, da kunden til dette projekt er så stor, kan man næsten ikke undgå at udvikle specielle programmer til de forksellige dele, som ligger et sted imellem devops og kubernetes, eller måske bliver en del af udviklingen af selve produktet. Det kunne være at kunden valgte ikke at betale, hvor vi så kunne gøre det sådan, at containere får færre og færre ressourcer som tiden går. -->

----
```bash
echo presentation.md > /dev/null
```
<!-- Det var det, tak for mig. -->